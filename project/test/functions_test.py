import unittest
from project.func.functions import add_numbers, return_last_char, to_lower_case, return_true

class FunctionsTests(unittest.TestCase):
    def test_add_numbers(self):
        self.assertEqual(add_numbers(1, 2), 3)

    def test_last_char(self):
        testString = "Test"
        self.assertEqual(return_last_char(testString), 't')

    def test_to_lower_case(self):
        testString = "SoMeHoW StRaNgE TeXt"
        self.assertEqual(to_lower_case(testString), testString.lower())

    def test_return_true(self):
        self.assertTrue(return_true())