"""Test package"""

def return_last_char(string):
    """last char"""
    return string[-1]

def to_lower_case(string):
    """to lower"""
    return string.lower()

def add_numbers(number_a, number_b):
    """add numbers"""
    output_value = number_a + number_b
    return output_value

def return_true():
    """return true"""
    return True
    